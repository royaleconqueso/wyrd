# Wyrd

*"Because you're tired of waiting for your bloated calendar program to start up."*

Wyrd is a text-based front-end to [Remind](https://dianne.skoll.ca/projects/remind/), a sophisticated calendar and alarm program serving two purposes:

1. It displays reminders in a scrollable timetable view suitable for visualizing your calendar at a glance.
2. It makes creating and editing reminders fast and easy.
   However, Wyrd does not hide Remind's textfile programmability, for this is what makes Remind a truly powerful calendaring system.

Wyrd also requires only a fraction of the resources of most calendar programs available today.

[Download](https://gitlab.com/wyrd-calendar/wyrd/-/jobs/artifacts/1.5.0/browse?job=release)

[Documentation](https://gitlab.com/wyrd-calendar/wyrd/-/jobs/artifacts/1.5.0/raw/manual.html?job=release)

Wyrd is Free Software; you can redistribute it and/or modify it under the terms of the GNU General Public License (GPL), Version 2, as published by the Free Software Foundation.
You should have received a copy of the GPL along with this program, in the file `COPYING`.

Wyrd website: https://gitlab.com/wyrd-calendar/wyrd
